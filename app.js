import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import generateEpub from './generateEpubs.js'

yargs(hideBin(process.argv))
  .scriptName('article-parser')
  .usage('$0 download <url> article and parse it to an epub')
  // .command('hello [dup]', 'parses an article at given url and formats it to an epub', (yargs) => {
  //   yargs.positional('dup', {
  //     type: 'string',
  //     default: "dupa",
  //     describe: 'article url'
  //   })
  // }, function (argv) {
  //   console.log('hello', argv.dup, 'welcome to yargs!')
  // })
  .command('$0 <url>', 'generate epub from an url', () => {},
   function (argv) {
    if(argv.url) {
      generateEpub(argv.url)
    }
  })
  .help()
  .argv
