import {extract} from 'article-parser'
import Epub from 'epub-gen'

export default function createEpub(url) {
 // const url = 'https://morgoth.substack.com/p/trans-them-all'
 extract(url).then((article) => {
    console.log(article)
  const options = {
    title: `${article.title}  `,
    author: article.author,
    output: `./epub/${article.title}.epub`,
    content: [
      {
        title: "About",
        data: `<div>Original article stored at <a href="${url}">${url}</a></div>`

      },
      {
        title: `${article.title}`,
        data: article.content
      }
    ]}

   new Epub(options).promise.then(() => console.log('Epub has been successufly generated'));

 }).catch((err) => {
   console.trace(err)
 })
}


